<?php

namespace core;

/**
 * Class Request - encapsulates a request and all associated data
 * @package core
 * @author Jonathan Wilkinson
 */
class Request
{
    private $_requestType;
    private $_data;
    private $_url;
    private static $_instance;

    /**
     * Constructor - populate the relevant request data
     * @access private
     */
    private function __construct()
    {
        $this->_requestType = $_SERVER['REQUEST_METHOD'];
        $this->_url = $_SERVER['REQUEST_URI'];

        foreach ($_GET as $key => $value)
        {
            $this->_data[$key] = $this->_sanitize($value);
        }

        foreach ($_POST as $key => $value)
        {
            $this->_data[$key] = $this->_sanitize($value);
        }
    }

    /**
     * Sanitize data as appropriate
     * @param $val
     * @return mixed
     */
    private function _sanitize($val)
    {
        return filter_var($val, FILTER_SANITIZE_STRING);
    }

    /**
     * Get an item from the request data if available
     * @param $key string Name of item to retrieve
     * @return mixed
     */
    public function get($key)
    {
        if (isset($this->_data[$key]))
        {
            return $this->_data[$key];
        }
    }

    /**
     * Get type of request made
     * @return string
     */
    public function getRequestMethod()
    {
        return $this->_requestType;
    }

    /**
     * Get instance
     * @return \core\Request Request instance
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance))
        {
            self::$_instance = new self;
        }

        return self::$_instance;
    }
}