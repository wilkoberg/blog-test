<?php

namespace core;

class Database
{
    private static $_pdoInstance;

    /**
     * Constructor
     * @access private - prevent instantiation
     */
    private function __construct()
    {

    }

    /**
     * Get instance
     * @return \PDO
     */
    public static function getConnection()
    {
        if (is_null(self::$_pdoInstance))
        {
            $settings = (include CONFIG_DIR . DS . 'database.php');
            self::$_pdoInstance = new \PDO('mysql:host=' . $settings['db_host'] . ';dbname=' . $settings['db_name'] . ';charset=utf8',
                $settings['db_user'], $settings['db_pwd']);
        }

        return self::$_pdoInstance;
    }
}