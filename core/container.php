<?php

namespace Core;

/**
 * Class Container - simple DI container
 * @package Core
 * @author Jonathan Wilkinson
 */
class Container extends \Pimple\Container
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $self = $this;

        $this['Request'] = function()
        {
            return \core\Request::getInstance();
        };

        $this['Loader'] = function()
        {
            return new \core\Loader();
        };

        $this['Router'] = function() use ($self)
        {
            return new \core\Router($self['Request'], $self['Loader']);
        };
    }

    /**
     * Resolve a dependency
     * @param $objectName
     * @return mixed
     * @throws \ErrorException
     */
    public static function make($objectName)
    {
        $container = new self;

        if (isset($container[$objectName]))
        {
            return $container[$objectName];
        }

        throw new \ErrorException("$objectName could not be loaded");
    }
}