<?php

namespace Core;

/**
 * Class Router - a simple routing class
 * @package Core
 * @author Jonathan Wilkinson
 */
class Router
{
    private $_request;
    private $_loader;

    /**
     * Constructor
     * @param Request $request
     * @param Loader $loader
     */
    public function __construct(\core\Request $request, \core\Loader $loader)
    {
        $this->_request = $request;
        $this->_loader = $loader;
    }

    /**
     * Dispatch route
     * @return array
     * @throws \NotFoundException Exception when a route cannot be fully resolved
     */
    public function dispatch()
    {
        // Basic routing - based upon a URL structure of /controller/action/
        $url = $this->_request->get('url');
        $routedUrl = !empty($url) ? urldecode($url) : '';
        $parts = explode('/', $routedUrl);
        $controller = isset($parts[0]) && !empty($parts[0]) ? $parts[0] : 'home';
        $action = isset($parts[1]) && !empty($parts[1]) ? $parts[1] . 'Action' : 'indexAction';

        // Remove controller and action - what's left is parameters
        unset($parts[0]);
        unset($parts[1]);

        $controllerObj = $this->_resolveController($controller);

        if (is_callable(array($controllerObj, $action)))
        {
            call_user_func_array(array($controllerObj, $action), $parts);
        }
        else
        {
            // Treat as a 404
            throw new \NotFoundException("{$url} cannot be found");
        }
    }

    /**
     * Resolve matched controller
     * @param $controllerName string Name of controller that has been resolved
     * @return mixed
     */
    private function _resolveController($controllerName)
    {
        $className = '\\controller\\' . ucfirst($controllerName . 'Controller');

        if (class_exists($className))
        {
            $controller = new $className($this->_request, $this->_loader);
        }
        else
        {
            $controller = $this->getErrorController();
        }

        return $controller;
    }

    /**
     * Instantiate the ErrorController
     * @return \controller\ErrorController ErrorController instance
     */
    public function getErrorController()
    {
        return new \controller\ErrorController($this->_request, $this->_loader);
    }
}