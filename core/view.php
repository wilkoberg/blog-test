<?php

namespace core;

/**
 * Class View - represents a HTML view
 * @package core
 */
class View
{
    private $_filename;
    private $_viewData;

    /**
     * Constructor
     * @param $templateName string
     * @param $viewData array
     * @access private
     */
    private function __construct($templateName, $viewData = array())
    {
        $this->_filename = $templateName;
        $this->_viewData = $viewData;
    }

    /**
     * Set an item of view data
     * @param $key string Key
     * @param $value mixed Value
     */
    public function set($key, $value)
    {
        $this->_viewData[$key] = $value;
    }

    /**
     * Render the view
     * @return string
     */
    public function render()
    {
        ob_start();
        ob_clean();
        extract($this->_viewData);
        require_once VIEW_DIR . DS . $this->_filename . '.php';
        $content = ob_get_clean();
        return $content;
    }

    /**
     * Make a view
     * @param $templateName string Template name
     * @param array $viewData View data to be set
     * @return new View instance
     */
    public static function make($templateName, $viewData = array())
    {
        return new self($templateName, $viewData);
    }

    /**
     * Render the view
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }
}