<?php

namespace core;

/**
 * Simple class loader/factory to avoid concrete
 * instantiations
 * @author Jonathan Wilkinson
 */

class Loader
{
    /**
     * Get a model instance
     * @param $modelName
     * @return mixed
     * @throws \Exception
     */
    public function loadModel($modelName)
    {
        if (class_exists('\\model\\' . $modelName))
        {
            $fqn = '\\model\\' . $modelName;
            $obj = new $fqn;
            $obj->setDatabase(\core\Database::getConnection());
            return $obj;
        }
        else
        {
            throw new \Exception("Cannot load $modelName");
        }
    }

    /**
     * Class autoloader
     * @param $className
     */
    public function loadClass($className)
    {
        $fileToTry = ROOT . DS . str_replace('\\', DS, strtolower($className) . '.php');

        if (file_exists($fileToTry))
        {
            include_once $fileToTry;
        }
    }
}