<div class="container">
    <h1>All Posts</h1>
    <a href="<?php echo SITE_URL; ?>/posts/new">New Post</a>
    <?php echo $message; ?>
    <?php if (!empty($posts)): ?>
        <table class="table">
            <thead>
            <tr>
                <th>Post Title</th>
                <th>Post Content Summary</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($posts as $post): ?>
                <tr>
                    <td><?php echo $post->post_title; ?></td>
                    <td><?php echo $post->truncated_content; ?></td>
                    <td>
                        <a href="<?php echo SITE_URL . "/posts/view/{$post->post_id}"; ?>">View</a>
                    </td>
                    <td>
                        <a href="<?php echo SITE_URL . "/posts/edit/{$post->post_id}"; ?>">Edit</a>
                    </td>
                    <td>
                        <a href="<?php echo SITE_URL . "/posts/delete/{$post->post_id}"; ?>">Delete</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>