<div id="footer">
    <div id="footer-top">
        <div class="container">
            <div class="column">
                <h3>Recent Posts</h3>
                <ul class="list-unstyled" id="recent-posts-list">
                    <li>Another Blog Post</li>
                    <li>Another Blog Post</li>
                    <li>Another Blog Post</li>
                    <li>Another Blog Post</li>
                </ul>
            </div>
            <div class="column">
                <h3>Social</h3>
                <ul class="list-unstyled" id="social-links-list">
                    <li>
                        <span class="text-hidden">Facebook</span>
                        <span class="icon">f</span>
                    </li>
                    <li class="bg-white">
                        <span class="text-hidden">Twitter</span>
                        <span class="icon">t</span>
                    </li>
                    <li>
                        <span class="text-hidden">Instagram</span>
                        <span class="icon">in</span>
                    </li>
                    <li>
                        <span class="text-hidden">LinkedIn</span>
                        <span class="icon">li</span>
                    </li>
                </ul>
            </div>
            <div class="column">
                <h3>About</h3>
                <p>
                    Etiam porta sem malesuada magna mollis euismod.
                    Duis mollis, est non commodo luctus, nisi erat porttitor
                    ligula, eget lacinia odio sem nec elit. Donec id elit non
                    mi porta gravida at eget metus. Sed posuere
                    consectetur est at lobortis.
                </p>
            </div>
        </div>
    </div>
    <div id="footer-bottom">
        <div id="footer-copyright" class="container">
            <div>
                &copy; <?php echo date('Y'); ?> <span>Blog Template</span>. All Rights Reserved.
                <div class="right">
                    <ul id="footer-nav-links" class="list-unstyled">
                        <li>
                            <a href="#">Terms of Use</a>
                        </li>
                        <li>
                            <a href="#">Privacy Policy</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>