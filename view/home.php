<div id="blog-posts-wrapper" class="container">
    <?php echo $message; ?>
    <?php foreach ($posts as $post): ?>
        <div class="blog-post">
            <div class="left media-container">
                <img src="<?php echo SITE_URL; ?>/assets/img/post-image.png" alt="Blog Post" />
            </div>
            <div class="left post-content-wrapper">
                <div class="post-info">
                    <span>Posted By: </span>
                    <span class="author">Blog Admin</span> / <?php echo $post->getPostTime(); ?> / <?php echo $post->getPostDate(); ?>
                </div>
                <h2 class="post-title"><?php echo $post->post_title; ?></h2>
                <div class="post-content">
                    <?php echo $post->truncated_content; ?>
                </div>
                <a href="<?php echo SITE_URL; ?>/posts/view/<?php echo $post->post_id; ?>" class="read-more-link">Read More...</a>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<div class="container">
    <a href="#" id="show-more-link">+ More Posts</a>
</div>