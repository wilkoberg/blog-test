<div class="container">
    <h1>Login</h1>
    <?php echo $message; ?>
    <form method="post" id="login-form">
        <div>
            <label for="user_name">User Name:</label>
            <input type="text" name="user_name" placeholder="User Name" class="form-control" />
        </div>
        <div>
            <label for="password">Password:</label>
            <input type="password" name="password" placeholder="Password" class="form-control" />
        </div>
        <div>
            <button type="submit" class="btn">Log In</button>
        </div>
    </form>
</div>