<!DOCTYPE html>
<html class="no-js" lang="en-GB" dir="ltr">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $pageTitle; ?></title>
        <meta name="viewport" content="width=device-width">
        <link href="//cdnjs.cloudflare.com/ajax/libs/normalize/3.0.2/normalize.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITE_URL; ?>/assets/css/main.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITE_URL; ?>/assets/css/mobile.css" rel="stylesheet" type="text/css" media="only screen and (max-width: 480px), only screen and (max-device-width: 480px)" />
        <!--[if lt IE 9]>
        <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="<?php echo SITE_URL; ?>/assets/js/main.js"></script>
    </head>
    <body>
        <div class="container">
            <?php echo $header; ?>
        </div>
        <div id="home-banner">
            <div id="banner-header">
                Blog Header
            </div>
            <p>
                Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
            </p>
        </div>
        <div id="main-content">
            <?php echo $content; ?>
        </div>
        <?php echo $footer; ?>
    </body>
</html>