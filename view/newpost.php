<div class="container">
    <h1>New Post</h1>
    <?php echo $message; ?>
    <form method="post" id="new-post-form">
        <fieldset>
            <div>
                <label for="post_title">Post Title</label>
                <input type="text" name="post_title" id="post_title" class="form-control" maxlength="50" value="<?php echo $post->post_title; ?>" />
            </div>
            <div>
                <label for="post_content">Post Content</label>
                <textarea name="post_content" id="post_content" class="form-control"><?php echo $post->post_content; ?></textarea>
            </div>
            <div>
                <input type="hidden" name="post_id" value="<?php echo $post->post_id; ?>" />
                <button class="btn">Publish Post</button>
            </div>
        </fieldset>
    </form>
</div>