<div id="main-header">
    <a href="<?php echo SITE_URL; ?>" title="<?php echo BLOG_NAME; ?>" id="home-link">
        <?php echo BLOG_NAME; ?>
    </a>
    <a href="#" id="nav-toggle">Menu</a>
    <ul id="header-nav" class="list-unstyled">
        <li>
            <a href="<?php echo SITE_URL; ?>" title="List">List</a>
        </li>
        <li>
            <a href="<?php echo SITE_URL; ?>/users/login" title="Admin">Admin</a>
        </li>
    </ul>
</div>