<?php

date_default_timezone_set('UTC');

if (!session_id())
{
    session_start();
}

// Constants
define('ROOT', __DIR__);
define('DS', DIRECTORY_SEPARATOR);
define('VIEW_DIR', __DIR__ . DS . 'view');
define('CONFIG_DIR', __DIR__ . DS . 'config');
define('CORE_DIR', __DIR__ . DS . 'core');

require_once __DIR__ . DS . 'vendor' . DS . 'autoload.php';
require_once CONFIG_DIR . DS . 'siteconfig.php';
require_once CORE_DIR . DS . 'loader.php';

$loader = new \core\Loader();

spl_autoload_register(array($loader, 'loadClass'));

// Initialise objects for this application
$request = \core\Container::make('Request');
$router = \core\Container::make('Router');

try
{
    $router->dispatch();
}
catch (\Exception $e)
{
    $controllerObj = $router->getErrorController();
    $controllerObj->show404($e);
}