<?php

namespace controller;

use controller\Controller;
use \core\View;

class HomeController extends Controller
{
    /**
     * Blog home page
     */
    public function indexAction()
    {
        $postsModel = $this->loader->loadModel('BlogPost');
        $recentPosts = $postsModel->getTopFivePosts();

        $view = View::make('home');
        $this->view->set('pageTitle', 'Recent Posts');
        $view->set('message', '');

        if (empty($recentPosts))
        {
            $view->set('message', '<p><strong>There are no posts to display</strong></p>');
        }

        $view->set('posts', $recentPosts);
        $this->view->set('content', $view);
        echo $this->view;
    }
}