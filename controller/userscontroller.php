<?php

namespace controller;

use \core\View;

class UsersController extends Controller
{
    public function loginAction()
    {
        if (isset($_SESSION['blog_user']))
        {
            header("Location: " . SITE_URL . '/posts/all');
            exit;
        }

        $userModel = $this->loader->loadModel('User');
        $view = View::make('login');
        $this->view->set('pageTitle', 'Login');
        $this->view->set('content', $view);
        $view->set('message', '');

        if ($this->request->getRequestMethod() == 'POST')
        {
            $username = $this->request->get('user_name');
            $password = $this->request->get('password');
            $hashedPassword = $userModel->hashPassword($password);

            if ($userModel->login($username, $hashedPassword))
            {
                header("Location: " . SITE_URL . '/posts/all');
                exit;
            }
            else
            {
                $view->set('message', '<p><strong>Your login details were not correct. Please try again</strong></p>');
            }
        }

        echo $this->view;
    }

    /**
     * Logout action
     */
    public function logoutAction()
    {
        $userModel = $this->loader->loadModel('User');
        $userModel->logout();
        header("Location: " . SITE_URL);
    }
}