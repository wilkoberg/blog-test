<?php

namespace controller;

use \core\Request;
use \core\View;
use \core\Loader;

abstract class Controller
{
    protected $view;
    protected $request;
    protected $loader;

    /**
     * Constructor
     * @param Request $request
     * @param Loader $loader
     */
    public function __construct(Request $request, Loader $loader)
    {
        // Dependencies injected
        $this->request = $request;
        $this->loader = $loader;
        $this->view = View::make('layout');

        $header = View::make('header');
        $footer = View::make('footer');
        $this->view->set('header', $header);
        $this->view->set('footer', $footer);
    }
}