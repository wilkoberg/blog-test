<?php

namespace controller;

use \core\View;

class PostsController extends Controller
{
    /**
     * Show all posts
     */
    public function allAction()
    {
        if (!isset($_SESSION['blog_user']))
        {
            header("Location: " . SITE_URL . '/users/login');
        }

        $blogModel = $this->loader->loadModel('BlogPost');
        $allPosts = $blogModel->get();
        $view = View::make('allposts');
        $view->set('message', '');
        $view->set('posts', $allPosts);

        if (empty($allPosts))
        {
            $view->set('message', '<p><strong>There are no posts to display</strong></p>');
        }

        $this->view->set('pageTitle', 'All Posts');
        $this->view->set('content', $view);
        echo $this->view;
    }

    /**
     * View a single post
     * @param null $postId
     */
    public function viewAction($postId = null)
    {
        $postModel = $this->loader->loadModel('BlogPost');

        if (!empty($postId))
        {
            $post = $postModel->getById($postId);

            if ($post != null)
            {
                $view = View::make('blogpost');
                $view->set('post', $post);
                $this->view->set('pageTitle', $post->post_title);
                $this->view->set('content', $view);
                echo $this->view;
            }
            else
            {
                throw new \NotFoundException("Post {$postId} could not be found");
            }
        }
        else
        {
            throw new \NotFoundException("Post could not be found");
        }
    }

    public function editAction($postId = null)
    {
        if (!isset($_SESSION['blog_user']))
        {
            header("Location: " . SITE_URL . '/users/login');
        }

        $postModel = $this->loader->loadModel('BlogPost');
        $view = View::make('editpost');
        $view->set('message', '');

        if (!empty($postId))
        {
            $post = $postModel->getById($postId);

            if ($post != null)
            {
                if ($this->request->getRequestMethod() == 'POST')
                {
                    $post->post_title = $this->request->get('post_title');
                    $post->post_content = $this->request->get('post_content');

                    if ($post->isValid())
                    {
                        if ($post->update())
                        {
                            $view->set('message', '<p><strong>Post has been updated!</strong></p>');
                        }
                        else
                        {
                            $view->set('message', '<p><strong>Post has not been updated! Please try again.</strong></p>');
                        }
                    }
                    else
                    {
                        $view->set('message', '<p><strong>Please enter a post title and post content.</strong></p>');
                    }
                }

                $view->set('post', $post);
                $this->view->set('pageTitle', 'Edit Post');
                $this->view->set('content', $view);
                echo $this->view;
            }
            else
            {
                throw new \NotFoundException("Post {$postId} could not be found");
            }
        }
        else
        {
            throw new \NotFoundException("Post could not be found");
        }
    }

    /**
     * Create a new post
     */
    public function newAction()
    {
        if (!isset($_SESSION['blog_user']))
        {
            header("Location: " . SITE_URL . '/users/login');
        }

        $post = $this->loader->loadModel('BlogPost');
        $view = View::make('newpost');
        $view->set('message', '');

        if ($this->request->getRequestMethod() == 'POST')
        {
            $post->post_title = $this->request->get('post_title');
            $post->post_content = $this->request->get('post_content');

            if ($post->isValid())
            {
                if ($post->insert())
                {
                    $view->set('message', '<p><strong>Your post has been published!</strong></p>');
                }
                else
                {
                    $view->set('message', '<p><strong>Your post could not be published! Please try again</strong></p>');
                }
            }
            else
            {
                $view->set('message', '<p><strong>Please enter a post title and content</strong></p>');
            }
        }

        $view->set('post', $post);
        $this->view->set('pageTitle', 'New Post');
        $this->view->set('content', $view);
        echo $this->view;
    }

    public function deleteAction($postId = null)
    {
        if (!isset($_SESSION['blog_user']))
        {
            header("Location: " . SITE_URL . '/users/login');
        }

        if ($postId != null)
        {
            $postModel = $this->loader->loadModel('BlogPost');
            $post = $postModel->getById($postId);

            if ($post != null)
            {
                $deleted = $post->delete();
                $url = $deleted ? "deleted=true" : "deleted=false";
                header("Location: " . SITE_URL . "/posts/all?{$url}");
                exit;
            }
            else
            {
                throw new \NotFoundException("Post could not be found");
            }
        }
        else
        {
            throw new \NotFoundException("Post could not be found");
        }
    }
}