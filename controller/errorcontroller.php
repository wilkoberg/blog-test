<?php

namespace controller;

use \core\View;

class ErrorController extends Controller
{
    public function show404($exception = null)
    {
        header("HTTP/1.0 404 Not Found");
        $view = View::make('404page');
        $this->view->set('pageTitle', 'Page/Resource Not Found');

        if ($exception instanceof \NotFoundException)
        {
            $view->set('message', $exception->getMessage());
        }
        else
        {
            $view->set('message', 'Sorry, but the page/resource you are looking for could not be found.');
        }

        $this->view->set('content', $view);
        echo $this->view;
    }
}