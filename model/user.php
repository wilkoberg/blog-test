<?php

namespace model;

class User extends Model
{
    protected $user_id;
    protected $user_name;
    protected $password;

    /**
     * Constructor
     * @param array $data
     */
    public function __construct($data = array())
    {
        if (!empty($data))
        {
            $this->user_id = $data['user_id'];
            $this->user_name = $data['user_name'];
            $this->password = $data['password'];
        }
    }

    /**
     * Login a user
     * @param $username
     * @param $password
     * @return bool
     */
    public function login($username, $password)
    {
        $stmt = $this->database->prepare('SELECT * FROM tbl_users WHERE user_name = :userName AND password = :password LIMIT 1');
        $stmt->bindValue(':userName', $username, \PDO::PARAM_STR);
        $stmt->bindValue(':password', $password, \PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        $stmt->closeCursor();

        if (!empty($row))
        {
            $userObj = new self($row);
            $_SESSION['blog_user'] = $userObj;
            return true;
        }

        return false;
    }

    public function logout()
    {
        session_destroy();
        unset($_COOKIE['PHPSESSID']);
    }

    /**
     * Hash a password
     * @param $rawPassword
     * @return string
     */
    public function hashPassword($rawPassword)
    {
        $salt = '9lcfckPJoaR&AUGZTON_k)ErJYL)3YT^$q-AtqU1';
        return hash('sha256', $rawPassword . $salt);
    }
}