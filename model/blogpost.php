<?php

namespace model;

/**
 * Class BlogPost - a single blog post
 * @package model
 * @author Jonathan Wilkinson
 */
class BlogPost extends Model
{
    protected $post_id;
    protected $post_title;
    protected $post_content;
    protected $create_time;
    protected $truncated_content;

    /**
     * Constructor
     * @param array $data - Data to populate record with
     */
    public function __construct($data = array())
    {
        if (!empty($data))
        {
            $this->post_id = $data['post_id'];
            $this->post_title = $data['post_title'];
            $this->post_content = $data['post_content'];
            $this->create_time = date('d F Y H:i:s', strtotime($data['create_time']));
            $this->truncated_content = substr($this->post_content, 0, 100);
        }
    }

    public function __get($key)
    {
        if (isset($this->$key))
        {
            return $this->$key;
        }

        return null;
    }

    public function __set($key, $value)
    {
        $this->$key = $value;
    }

    /**
     * Get the last 5 posts
     * @return array
     */
    public function getTopFivePosts()
    {
        return $this->get(5);
    }

    /**
     * Get posts
     * @param null $limit
     * @return array
     */
    public function get($limit = null)
    {
        $query = 'SELECT * FROM tbl_blog_posts ORDER BY create_time DESC ';

        if (is_numeric($limit))
        {
            $query .= "LIMIT 0, $limit";
        }

        $stmt = $this->database->prepare($query);
        $stmt->execute();
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        $posts = array();

        foreach ($rows as $record)
        {
            $post = new self($record);
            $post->setDatabase($this->database);
            $posts[] = $post;
        }

        return $posts;
    }

    /**
     * Insert a new blog post
     * @param array $data Data to add
     * @return bool True if added, false if not
     */
    public function insert()
    {
        $query = 'INSERT INTO tbl_blog_posts (post_title, create_time, post_content, last_updated) VALUES (:title, NOW(), :content, NOW())';
        $stmt = $this->database->prepare($query);
        $stmt->bindValue(':title', $this->post_title, \PDO::PARAM_STR);
        $stmt->bindValue(':content', $this->post_content, \PDO::PARAM_STR);
        $stmt->execute();
        $errorInfo = $this->database->errorInfo();
        $isSuccess = $errorInfo[0] == '00000';

        if ($isSuccess)
        {
            // Get last insert ID and set
            $this->post_id = $this->database->lastInsertId();
        }

        return $isSuccess;
    }

    /**
     * Update a blog post
     * @return bool
     */
    public function update()
    {
        $query = 'UPDATE tbl_blog_posts SET post_title = :postTitle, post_content = :postContent, last_updated = NOW() WHERE post_id = :postId LIMIT 1';
        $stmt = $this->database->prepare($query);
        $stmt->bindValue(':postTitle', $this->post_title, \PDO::PARAM_STR);
        $stmt->bindValue(':postContent', $this->post_content, \PDO::PARAM_STR);
        $stmt->bindValue(':postId', $this->post_id, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->rowCount();
    }

    /**
     * Delete from database
     * @return bool
     */
    public function delete()
    {
        $query = "DELETE FROM tbl_blog_posts WHERE post_id = :postId LIMIT 1";
        $stmt = $this->database->prepare($query);
        $stmt->bindValue('postId', $this->post_id, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->rowCount();
    }

    /**
     * Get an item by ID
     * @param $id int Post ID
     * @return mixed
     */
    public function getById($id)
    {
        $query = 'SELECT * FROM tbl_blog_posts WHERE post_id = :postId LIMIT 1';
        $stmt = $this->database->prepare($query);
        $stmt->bindValue(':postId', $id, \PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (!empty($row))
        {
            $post = new self($row);
            $post->setDatabase($this->database);
            return $post;
        }

        return null;
    }

    /**
     * Return if the post is valid
     * @return bool
     */
    public function isValid()
    {
        return !empty($this->post_content) && !empty($this->post_title);
    }

    /**
     * Get post date
     * @return string
     */
    public function getPostDate()
    {
        return date('d M Y', strtotime($this->create_time));
    }

    /**
     * Get post time
     * @return string
     */
    public function getPostTime()
    {
        return date('H:i', strtotime($this->create_time));
    }
}