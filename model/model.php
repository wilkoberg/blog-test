<?php

namespace model;

abstract class Model
{
    protected $database;

    /**
     * Set DB connection
     * @param \PDO $pdoInstance
     */
    public function setDatabase($pdoInstance)
    {
        $this->database = $pdoInstance;
    }
}