$(function() {
    var headerNav = $('#header-nav');

    $('#nav-toggle').on('click', function(e) {
        e.preventDefault();

        if (headerNav.is(':hidden')) {
            headerNav.slideDown('slow');
        } else {
            headerNav.slideUp('slow');
        }
    });

    $(window).resize(function() {
        if ($(window).width() > 480) {
            headerNav.removeAttr('style'); // Clear styles applied from mobile nav
        }
    });
});